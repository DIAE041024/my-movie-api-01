from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from schemas.category import Category
from services.category_service import CategoryService
from utils.jwt_manager import decode_access_token
from database import get_db

router = APIRouter(
    prefix="/categories",
    tags=["categories"],
    dependencies=[Depends(decode_access_token)]
)

@router.post("/", response_model=Category, status_code=status.HTTP_201_CREATED)
def create_category(category: Category, db: Session = Depends(get_db)):
    return CategoryService(db).create_category(category)

@router.put("/{id}", response_model=Category)
def update_category(id: int, category: Category, db: Session = Depends(get_db)):
    return CategoryService(db).update_category(id, category)

@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_category(id: int, db: Session = Depends(get_db)):
    CategoryService(db).delete_category(id)
