from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from schemas.movie import Movie
from services.movie_service import MovieService
from utils.jwt_manager import decode_access_token
from database import get_db

router = APIRouter(
    prefix="/movies",
    tags=["movies"],
    dependencies=[Depends(decode_access_token)]
)

@router.post("/", response_model=Movie, status_code=status.HTTP_201_CREATED)
def create_movie(movie: Movie, db: Session = Depends(get_db)):
    return MovieService(db).create_movie(movie)

@router.get("/{id}", response_model=Movie)
def get_movie(id: int, db: Session = Depends(get_db)):
    movie = MovieService(db).get_movie(id)
    if movie is None:
        raise HTTPException(status_code=404, detail="Movie not found")
    return movie

@router.get("/", response_model=list[Movie])
def get_movies(db: Session = Depends(get_db)):
    return MovieService(db).get_movies()

@router.get("/category/{category}", response_model=list[Movie])
def get_movies_by_category(category: str, db: Session = Depends(get_db)):
    return MovieService(db).get_movies_by_category(category)

@router.put("/{id}", response_model=Movie)
def update_movie(id: int, movie: Movie, db: Session = Depends(get_db)):
    return MovieService(db).update_movie(id, movie)

@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_movie(id: int, db: Session = Depends(get_db)):
    MovieService(db).delete_movie(id)
