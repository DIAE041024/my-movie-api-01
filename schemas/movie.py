from pydantic import BaseModel

class Movie(BaseModel):
    id: int
    title: str
    description: str
    category_id: int

    class Config:
        orm_mode = True
