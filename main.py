from fastapi import FastAPI
from routers import movie, category
from database import engine
from models import movie as movie_model
from models import category as category_model

# Crear las tablas en la base de datos
movie_model.Base.metadata.create_all(bind=engine)
category_model.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(movie.router)
app.include_router(category.router)

@app.get("/")
def read_root():
    return {"message": "Welcome to the Movie API"}

    ##Increíble
