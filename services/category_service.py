from sqlalchemy.orm import Session
from models.category import CategoryModel
from schemas.category import Category
from models.movie import MovieModel
from fastapi import HTTPException

class CategoryService:
    def __init__(self, db: Session):
        self.db = db

    def create_category(self, category: Category):
        db_category = CategoryModel(**category.dict())
        self.db.add(db_category)
        self.db.commit()
        self.db.refresh(db_category)
        return db_category

    def update_category(self, id: int, category: Category):
        db_category = self.db.query(CategoryModel).filter(CategoryModel.id == id).first()
        if db_category:
            for key, value in category.dict().items():
                setattr(db_category, key, value)
            self.db.commit()
            self.db.refresh(db_category)
            return db_category
        return None

    def delete_category(self, id: int):
        db_movie = self.db.query(MovieModel).filter(MovieModel.category_id == id).first()
        if db_movie:
            raise HTTPException(status_code=400, detail="Category is assigned to a movie")
        db_category = self.db.query(CategoryModel).filter(CategoryModel.id == id).first()
        if db_category:
            self.db.delete(db_category)
            self.db.commit()
