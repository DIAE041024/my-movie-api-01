from sqlalchemy.orm import Session
from models.movie import MovieModel
from schemas.movie import Movie
from models.category import CategoryModel

class MovieService:
    def __init__(self, db: Session):
        self.db = db

    def create_movie(self, movie: Movie):
        category = self.db.query(CategoryModel).filter(CategoryModel.id == movie.category_id).first()
        if not category:
            raise HTTPException(status_code=400, detail="Category does not exist")
        db_movie = MovieModel(**movie.dict())
        self.db.add(db_movie)
        self.db.commit()
        self.db.refresh(db_movie)
        return db_movie

    def get_movie(self, id: int):
        return self.db.query(MovieModel).filter(MovieModel.id == id).first()

    def get_movies(self):
        return self.db.query(MovieModel).all()

    def get_movies_by_category(self, category: str):
        return self.db.query(MovieModel).filter(MovieModel.category == category).all()

    def update_movie(self, id: int, movie: Movie):
        db_movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        if db_movie:
            category = self.db.query(CategoryModel).filter(CategoryModel.id == movie.category_id).first()
            if not category:
                raise HTTPException(status_code=400, detail="Category does not exist")
            for key, value in movie.dict().items():
                setattr(db_movie, key, value)
            self.db.commit()
            self.db.refresh(db_movie)
            return db_movie
        return None

    def delete_movie(self, id: int):
        db_movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        if db_movie:
            self.db.delete(db_movie)
            self.db.commit()
